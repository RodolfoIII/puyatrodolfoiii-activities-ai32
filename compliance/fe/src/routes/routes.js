import Vue from 'vue'
import VueRouter from 'vue-router'
import Inventory from "../components/views/Inventory.vue";
import Transactions from "../components/views/Transactions.vue";
import editProduct from "../components/views/editProduct.vue";
import sellProduct from "../components/views/sellProduct.vue";





Vue.use(VueRouter)

export default new VueRouter({
    mode: "history",
    routes: [
      {
        path: "/",
        component:Inventory,
      },

      {
        path: "/Inventory",
        name: 'Inventory',
        component:Inventory,
      },
      {
        path: "/Transactions",
        name: 'Transactions',
        component: Transactions,
      },

      {
        path: "/sellProduct",
        name: 'sellProduct',
        component: sellProduct,
      },
      {
        path: "/editProduct",
        name: 'editProduct',
        component: editProduct,
      }
      
    ]
});
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function getProduct() {
        $product = Product::all();
        return $product;
    }

    public function updateProduct($id){
        $products = Product::find($id);
        return response()->json($products);
    }

    public function saveProduct(Request $request) {
        $products = new Product;

        $products->product_name          = $request->product_name;
        $products->quantity              = $request->quantity;
        $products->price                 = $request->price;
        $products->Brand                 = $request->Brand;
        $products->logistic_company      = $request->logistic_company;
        $products->personel              = $request->personel;

        if($products->save()) {
            return response()->json(['status' => true, 'message'  =>'Products Added Successfully']);
        }
        else{
            return response()->json(['status' => false, 'message'  =>'There is some problem. Please try it again']); 
        }
        }


        public function update_Product(Request $request, $id) {
            $products = Product::where('id',$id)->first();
    
            $products->product_name          = $request->product_name;
            $products->quantity              = $request->quantity;
            $products->price                 = $request->price;
            $products->Brand                 = $request->Brand;
            $products->logistic_company      = $request->logistic_company;
            $products->personel              = $request->personel;
    
            if($products->save()) {
                return response()->json(['status' => true, 'message'  =>'Product updated Successfully']);
            }
            else{
                return response()->json(['status' => false, 'message'  =>'There is some problem. Please try it again']); 
            }
            }
    

        public function deleteProduct($id) {
            $product =  Product::find($id);
            if($product->delete()) {
                return response()->json(['status' => true,'message' => 'Product Deleted Successfully']);
            }
            else{
                return response()->json(['status' => false,'message' => 'Something Went Wrong']);
            }
        }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;

class TransactionController extends Controller
{


    public function getTransaction() {
        $transaction = Transaction::all();
        return $transaction;
    }

    public function saveTransaction(Request $request) {
        $transactions = new Transaction;

        $transactions->product_name          = $request->product_name;
        $transactions->quantity              = $request->quantity;
        $transactions->description           = $request->description;
        $transactions->personel              = $request->personel;

        if($transactions->save()) {
            return response()->json(['status' => true, 'message'  =>'Transaction Added Successfully']);
        }
        else{
            return response()->json(['status' => false, 'message'  =>'There is some problem. Please try it again']); 
        }
        }



        public function deleteTransaction($id) {
            $transaction =  Transaction::find($id);
            if($transaction->delete()) {
                return response()->json(['status' => true,'message' => 'Transaction Deleted Successfully']);
            }
            else{
                return response()->json(['status' => false,'message' => 'Something Went Wrong']);
            }
        }

}



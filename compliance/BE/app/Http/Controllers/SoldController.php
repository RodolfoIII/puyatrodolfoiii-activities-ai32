<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sold;

class SoldController extends Controller
{

    public function getSold() {
        $sold = Sold::all();
        return $sold;
    }

    public function soldProduct(Request $request) {
        $sold = new Sold;

        $sold->product_name          = $request->product_name;
        $sold->quantity              = $request->quantity;
        $sold->price                 = $request->price;
        $sold->Brand                 = $request->Brand;

        if($sold->save()) {
            return response()->json(['status' => true, 'message'  =>'Sold products Added Successfully']);
        }
        else{
            return response()->json(['status' => false, 'message'  =>'There is some problem. Please try it again']); 
        }
        }

}

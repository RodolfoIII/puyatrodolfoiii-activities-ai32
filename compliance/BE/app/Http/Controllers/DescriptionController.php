<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Description;

class DescriptionController extends Controller
{
    public function getDescription(){
        $description = Description::get();
        return response()->json($description);
    }
}

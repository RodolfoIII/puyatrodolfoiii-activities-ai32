require('./bootstrap');
window.Vue  = require('vue');


import VueRouter from 'vue-router';
Vue.use(VueRouter);

import App from './App.vue';
import Inventory from '../js/components/Inventory';
import Transaction from '../js/components/Transaction';
import editProduct from '../js/components/editProduct';
import sellProduct from '../js/components/sellProduct';


//sweetalert
import VueSweetalert2 from 'vue-sweetalert2';
window.Swal = require('sweetalert2');
Vue.use(VueSweetalert2);

import utils from './helpers/utilities';
Vue.prototype.$utils = utils

import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'

window.toastr=require('toastr')

Vue.use(VueToastr2)



import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)



import VueAxios from 'vue-axios';
import axios from 'axios';
import Vue from 'vue';

Vue.use(VueAxios, axios);

const routes  = [
    {
        name:'/',
        path:'/',
        component:Inventory
    },
    
    {
        name:'/transaction',
        path:'/transaction',
        component:Transaction
    },
    {
        name:'/updateProduct',
        path:'/updateProduct/edit/:id?',
        component:editProduct
    },
    {
        name:'/sellprod',
        path:'/sellprod/edit/:id?',
        component:sellProduct
    }

];



const router  = new VueRouter({mode: 'history',routes: routes});
const app  = new Vue(Vue.util.extend({router},App)).$mount('#app');
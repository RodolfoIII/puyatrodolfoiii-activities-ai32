<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


  //PRODUCT

  //get product
  Route::get('getProduct' ,'App\Http\Controllers\ProductController@getProduct');

  // get product Detail
  Route::get('updateProduct/{id}','App\Http\Controllers\ProductController@updateProduct');

  // save product
  Route::post('saveProduct','App\Http\Controllers\ProductController@saveProduct');

  // update Product
  Route::post('update_Product/{id}','App\Http\Controllers\ProductController@update_Product');

  // Delete product
  Route::delete('deleteProduct/{id}','App\Http\Controllers\ProductController@deleteProduct');

  //get Dscription
  Route::get('getDescription','App\Http\Controllers\DescriptionController@getDescription'); 

  
//TRANSACTION
// save Transaction
Route::post('saveTransaction','App\Http\Controllers\TransactionController@saveTransaction');

//get Transactions
Route::get('getTransaction' ,'App\Http\Controllers\TransactionController@getTransaction');

// Delete Transaction
Route::delete('deleteTransaction/{id}','App\Http\Controllers\TransactionController@deleteTransaction');


//Sold Products
Route::post('soldProduct','App\Http\Controllers\SoldController@soldProduct');

//get Sold Products
Route::get('getSold' ,'App\Http\Controllers\SoldController@getSold');